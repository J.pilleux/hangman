package main

import (
	"fmt"
	"os"
	"udemy/hangman/dictionary"
	"udemy/hangman/hangman"
)

func main() {
	err := dictionary.Load("words.txt")
	if err != nil {
		fmt.Printf("Could not load dictionary: %v\n", err)
		os.Exit(1)
	}

	g, err := hangman.New(8, dictionary.PickWord())
	if err != nil {
		fmt.Printf("Could not initialize game: %v\n", err)
		os.Exit(1)
	}

	guess := ""
	for {
		hangman.ClearScreen()

		hangman.DrawWelcome()
		hangman.Draw(g, guess)

		switch g.State {
		case "won", "lost":
			os.Exit(0)
		}

		l, err := hangman.ReadGuess()
		if err != nil {
			fmt.Printf("Could not read from terminal: %v\n", err)
			os.Exit(1)
		}
		guess = l
		g.MakeAGuess(guess)
	}
}
