package hangman

import (
	"fmt"
	"os"
	"os/exec"
)

func DrawWelcome() {
	fmt.Println(`
 _   _   ___   _   _ _____ ___  ___  ___   _   _
| | | | / _ \ | \ | |  __ \|  \/  | / _ \ | \ | |
| |_| |/ /_\ \|  \| | |  \/| .  . |/ /_\ \|  \| |
|  _  ||  _  || . ' | | __ | |\/| ||  _  || . ' |
| | | || | | || |\  | |_\ \| |  | || | | || |\  |
\_| |_/\_| |_/\_| \_/\____/\_|  |_/\_| |_/\_| \_/
	`)
}

func ClearScreen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func Draw(g *Game, guess string) {
	drawTurns(g.TurnsLeft)
	drawState(g, guess)
}

func drawTurns(l int) {
	var draw string
	switch l {
	case 0:
		draw = `
________
|/   |
|   (_)
|   /|\
|    |
|   / \
|
|___
		`
	case 1:
		draw = `
________
|/   |
|   (_)
|   /|\
|    |
|   /
|
|___
		`
	case 2:
		draw = `
________
|/   |
|   (_)
|   /|\
|    |
|
|
|___
		`
	case 3:
		draw = `
________
|/   |
|   (_)
|   /|
|    |
|
|
|___
		`
	case 4:
		draw = `
________
|/   |
|   (_)
|    |
|    |
|
|
|___
		`
	case 5:
		draw = `
________
|/   |
|   (_)
|
|
|
|
|___
		`
	case 6:
		draw = `
________
|/   |
|
|
|
|
|
|___
		`
	case 7:
		draw = `
________
|/
|
|
|
|
|
|___
		`
	case 8:
		draw = `

		`
	}
	fmt.Println(draw)
}

func drawState(g *Game, guess string) {
	fmt.Print("Guessed:	")
	drawLetters(g.FoundLetters)

	fmt.Print("Used: ")
	drawLetters(g.UsedLetters)

	switch g.State {
	case "goodGuess":
		fmt.Println("Good guess!")
	case "alreadyGuessed":
		fmt.Printf("Letters '%s' was already used\n", guess)
	case "badGuess":
		fmt.Printf("Bad guess, '%s' is not in the word\n", guess)
	case "lost":
		fmt.Print("You lost :(! The word was: ")
		drawLetters(g.Letters)
	case "won":
		fmt.Print("YOU WON! The word was: ")
		drawLetters(g.Letters)
	}
}

func drawLetters(l []string) {
	for _, c := range l {
		fmt.Printf("%v ", c)
	}
	fmt.Println()
}
